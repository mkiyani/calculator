Symfony Calculator Application
========================

The "Mr&Mrs Smith Calculator test" is a reference application that can handle the arithmetical (plus, minus, multiplication, division) and bitwise (AND, OR) operations.

Requirements
------------

  * PHP 5.5.9 or higher;
  * and the [usual Symfony application requirements][1].

Installation
------------

Execute this command to install the vendor packages:

```bash
$ composer install
```

Usage
-----

There's no need to configure anything to run the application. Just execute this
command to run the built-in web server and access the application in your:

```bash
$ cd calculator/
$ php bin/console server:run
```

Alternatively, you can [configure a fully-featured web server][2] like Nginx
or Apache to run the application.


[1]: https://symfony.com/doc/current/reference/requirements.html
[2]: https://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html