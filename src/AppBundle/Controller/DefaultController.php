<?php

namespace AppBundle\Controller;

use AppBundle\Form\CalculatorType;
use AppBundle\Model\Operation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 *
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \AppBundle\Model\Operation $operation
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, Operation $operation)
    {
        $form = $this->createForm(
            CalculatorType::class,
            $operation,
            ['attr' => ['class' => 'form-inline']]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $result = $operation->compute();
            } catch (\Exception $ex) {
                $result = $ex->getMessage();
            }


            return $this->render('default/index.html.twig', [
              'result' => $result,
              'form' => $form->createView(),
            ]);
        }

        return $this->render('default/index.html.twig', [
          'form' => $form->createView(),
        ]);
    }
}
