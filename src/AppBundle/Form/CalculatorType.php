<?php

namespace AppBundle\Form;

use AppBundle\Model\Operation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CalculatorType
 *
 * @package AppBundle\Form
 */
class CalculatorType extends AbstractType
{

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('operand_1', NumberType::class, [
            'label' => false,
            'required' => true,
            'attr' => ['placeholder' => 'Operand1'],
          ])
          ->add('operator', ChoiceType::class, [
            'choices' => [
              '+ (plus)' => 'plus',
              '- (minus)' => 'minus',
              'x (multiply)' => 'multiply',
              '÷ (divide)' => 'divide',
              '| (OR)' => 'OR',
              '& (AND)' => 'AND',
            ],
            'label' => false,
          ])
          ->add('operand_2', NumberType::class, [
            'label' => false,
            'required' => true,
            'attr' => ['placeholder' => 'Operand2'],
          ])
          ->add('submit', SubmitType::class, ['label' => '=']);
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
          'data_class' => Operation::class,
        ]);
    }
}
