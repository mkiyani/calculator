<?php

namespace AppBundle\Service;

use AppBundle\Model\Operator\BWAND;
use AppBundle\Model\Operator\BWOR;
use AppBundle\Model\Operator\Divide;
use AppBundle\Model\Operator\Minus;
use AppBundle\Model\Operator\Multiply;
use AppBundle\Model\Operator\Plus;

/**
 * Class OperationFactory
 *
 * @package AppBundle\Service
 */
class OperationFactory
{

    /**
     * @param string $operator
     *
     * @return \AppBundle\Model\Operator\Operator
     * @throws \Exception
     */
    public function getOperator($operator)
    {
        $result = null;
        switch ($operator) {
            case 'plus':
                return new Plus();
            case 'minus':
                return new Minus();
            case 'multiply':
                return new Multiply();
            case 'divide':
                return new Divide();
            case 'OR':
                return new BWOR();
            case 'AND':
                return new BWAND();

            default:
                throw new \Exception("$operator operator invalid.");
        }
    }
}
