<?php

namespace AppBundle\Model;

use AppBundle\Service\OperationFactory;

/**
 * Class Operation
 *
 * @package AppBundle\Model
 */
class Operation
{

    /**
     * @var int
     */
    protected $operand_1;

    /**
     * @var int
     */
    protected $operand_2;

    /**
     * @var string
     */
    protected $operator;

    /**
     * @var OperationFactory
     */
    private $factory;

    /**
     * Operation constructor.
     *
     * @param \AppBundle\Service\OperationFactory $factory
     */
    public function __construct(OperationFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return double|int
     * @throws \Exception
     */
    public function compute()
    {
        $result = null;

        $obj = $this->factory->getOperator($this->getOperator());

        try {
            return $obj->compute($this->getOperand1(), $this->getOperand2());
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return double
     */
    public function getOperand1()
    {
        return $this->operand_1;
    }

    /**
     * @param double $operand_1
     *
     * @throws \Exception
     */
    public function setOperand1($operand_1)
    {
        if (!is_numeric($operand_1)) {
            throw new \Exception("Invalid entry.");
        }

        $this->operand_1 = $operand_1;
    }

    /**
     * @return double
     */
    public function getOperand2()
    {
        return $this->operand_2;
    }

    /**
     * @param double $operand_2
     *
     * @throws \Exception
     */
    public function setOperand2($operand_2)
    {
        if (!is_numeric($operand_2)) {
            throw new \Exception("Invalid entry.");
        }

        $this->operand_2 = $operand_2;
    }
}
