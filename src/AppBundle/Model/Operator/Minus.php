<?php

namespace AppBundle\Model\Operator;

/**
 * Class Minus
 *
 * @package AppBundle\Model\Operator
 */
class Minus implements Operator
{

    /**
     * @param double $operand1
     * @param double $operand2
     *
     * @return double|int
     */
    public function compute($operand1, $operand2)
    {
        return $operand1 - $operand2;
    }
}
