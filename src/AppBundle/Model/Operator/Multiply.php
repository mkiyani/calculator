<?php

namespace AppBundle\Model\Operator;

/**
 * Class Multiply
 *
 * @package AppBundle\Model\Operator
 */
class Multiply implements Operator
{

    /**
     * @param double $operand1
     * @param double $operand2
     *
     * @return double|int
     */
    public function compute($operand1, $operand2)
    {
        return $operand1 * $operand2;
    }
}
