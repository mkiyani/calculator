<?php

namespace AppBundle\Model\Operator;

/**
 * Class BWAND
 *
 * @package AppBundle\Model\Operator
 */
class BWAND implements Operator
{

    /**
     * @param double $operand1
     * @param double $operand2
     *
     * @return int
     */
    public function compute($operand1, $operand2)
    {
        return $operand1 & $operand2;
    }

}
