<?php

namespace AppBundle\Model\Operator;

/**
 * Class Divide
 *
 * @package AppBundle\Model\Operator
 */
class Divide implements Operator
{

    /**
     * @param double $operand1
     * @param double $operand2
     *
     * @return double|int
     * @throws \Exception
     */
    public function compute($operand1, $operand2)
    {

        if ($operand2 == 0) {
            throw new \Exception("Division by zero.");
        }
        return $operand1 / $operand2;
    }
}
