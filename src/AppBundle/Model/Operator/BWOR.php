<?php

namespace AppBundle\Model\Operator;

/**
 * Class BWOR
 *
 * @package AppBundle\Model\Operator
 */
class BWOR implements Operator
{

    /**
     * @param double $operand1
     * @param double $operand2
     *
     * @return int
     */
    public function compute($operand1, $operand2)
    {
        return $operand1 | $operand2;
    }

}
