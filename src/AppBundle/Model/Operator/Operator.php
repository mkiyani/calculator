<?php

namespace AppBundle\Model\Operator;

/**
 * Interface Operator
 *
 * @package AppBundle\Model\Operator
 */
interface Operator
{

    /**
     * @param double $operand1
     * @param double $operand2
     *
     * @return double|int
     */
    public function compute($operand1, $operand2);
}
