<?php

namespace AppBundle\Model\Operator;

/**
 * Class Plus
 *
 * @package AppBundle\Model\Operator
 */
class Plus implements Operator
{

    /**
     * @param double $operand1
     * @param double $operand2
     *
     * @return double|int
     */
    public function compute($operand1, $operand2)
    {
        return $operand1 + $operand2;
    }

}
